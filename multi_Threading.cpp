#include <iostream>
#include <thread>
#include <random>
#include <time.h>
#include <unistd.h>
#include <math.h>
#include <bits/stdc++.h>
#include <mutex>
#define vs vector<string>
#define vth vector<thread>
#define vi vector<int>
#define pb push_back
using namespace std;

#pragma once

class elevatorSystem
{
public:
	int order;			   // ten thang may
	int curFloor;		   // tang hien tai cua thang may
	int ammount;		   // so nguoi hien tai trong thang may
	vector<string> status; // cac trang thai cua thang may
	int maximum;		   // so nguoi toi da trong thang may
	int direct;			   // huong chay cua thang may
	string tmp;			   // trang thai hien tai cua thang may
	elevatorSystem(){};
	~elevatorSystem(){};
	void init()
	{
		ammount = 0;
		curFloor = 10;
		status.pb("OPEN1");	 // 1
		status.pb("CLOSE1"); // 2
		status.pb("OPEN2");	 // 3
		status.pb("CLOSE2"); // 4
		status.pb("UP1");	 // 5
		status.pb("DOWN1");	 // 6
		status.pb("UP2");	 // 7
		status.pb("DOWN2");	 // 8
		status.pb("IDLE");	 // 9
		tmp = status[8];	 // 10
	};
};
class directStatus
{
public:
	int up, down, open, close;
};
class Floors
{
public:
	int beginFloor;
	int finalFloor;
	Floors(){};
	~Floors(){};
};

int main()
{
	elevatorSystem p;
	p.init();
	return 0;
}